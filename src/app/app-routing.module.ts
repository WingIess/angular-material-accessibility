import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { AuthGuard } from './guards/authentication.guard';

const routes: Routes = [
  { path: '', component: HomeComponent, children: [] },
  { path: 'page-2', component: Page2Component },
  { path: 'page-3', component: Page3Component, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
