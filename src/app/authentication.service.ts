import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  authenticated: Subject<boolean> = new Subject();
  loggedIn: boolean = false;
  userName: string = "";
  password: string = "";

  constructor() {
    this.loginStatusChange(false);
  }

  loginStatusChange(newStatus: boolean) {
    this.loggedIn = newStatus;
    this.authenticated.next(newStatus);
  }

  login(userName: string, password: string) {
    setTimeout(() => {
      this.loginStatusChange(true);
      this.userName = userName;
      this.password = password;
    }, 600);
  }

  logout() {
    this.loginStatusChange(false);
    this.userName = "";
    this.password = "";
  }
}
