import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MatDialog } from "@angular/material/dialog";
import { AuthenticationService } from "src/app/authentication.service";

@Component({
  selector: "app-login-modal",
  templateUrl: "./login-modal.component.html",
  styleUrls: ["./login-modal.component.scss"],
})
export class LoginModalComponent implements OnInit, OnDestroy {
  @ViewChild("form") formRef!: NgForm;
  username: string = "";
  password: string = "";
  loading: boolean = false;
  showPassword: boolean = false;

  constructor(private autheticationService: AuthenticationService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.autheticationService.authenticated.subscribe((loggeddIn) => {
      if (loggeddIn) {
        this.loading = false;
        this.handleCloseModal();
      } else {
        // TODO: handle logout
      }
    });
  }

  ngOnDestroy() {
    this.autheticationService.authenticated.unsubscribe();
  }

  loginHandler() {
    if (this.formRef.touched && this.formRef.valid && this.formRef.dirty) {
      this.loading = true;
      this.autheticationService.login(this.username, this.password);
    }
  }

  handleVisibilityChange() {
    this.showPassword = !this.showPassword;
  }

  handleCloseModal() {
    this.dialog.closeAll();
  }
}
