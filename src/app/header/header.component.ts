import { Component, OnDestroy, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { AuthenticationService } from "../authentication.service";
import { LoginModalComponent } from "./login-modal/login-modal.component";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit, OnDestroy {
  loggedIn: boolean = false;

  constructor(private authenticationService: AuthenticationService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.authenticationService.authenticated.subscribe((data) => {
      this.loggedIn = data;
    });
  }

  ngOnDestroy() {
    this.authenticationService.authenticated.unsubscribe();
  }

  authenticationHandler(): void {
    if (this.loggedIn) {
      this.authenticationService.logout();
    } else {
      this.dialog.open(LoginModalComponent);
    }
  }
}
