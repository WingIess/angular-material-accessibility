import { Component, OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "../authentication.service";

@Component({
  selector: "app-page3",
  templateUrl: "./page3.component.html",
  styleUrls: ["./page3.component.scss"],
})
export class Page3Component implements OnInit, OnDestroy {
  constructor(private router: Router, private authenticationService: AuthenticationService) {}

  ngOnInit(): void {
    this.authenticationService.authenticated.subscribe((loggedIn) => {
      if (!loggedIn) this.router.navigate(["/"]);
    });
  }

  ngOnDestroy() {
    this.authenticationService.authenticated.unsubscribe();
  }
}
